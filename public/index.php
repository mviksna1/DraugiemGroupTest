<?php



interface DataInterface
{

}

class CSV implements DataInterface
{
	public $url;
	public function __construct($url) {
		$this->url = $url;
	}
	public function run()
	{
		$handle = fopen("http://www.printful.com/test.csv", "r") or die("Couldn't get handle");
		$a = 1;
		$items = new Items;

		if ($handle) {
			
		    while (!feof($handle)) {
		        $buffer = fgets($handle, 8096);

		        $items->add(new Item($buffer));

		        if ($a++ === 2) {
		        	break;
		        }
		    }
		    fclose($handle);

		    
		}

		return $items;
	}
}


class CollectData
{
	public $object;

	public $items;

	public function __construct(DataInterface $object)
	{
		$this->object = $object;
	}

	public function run()
	{
		$this->items = $this->object->run();
	}

	public function drawTable()
	{
		$this->items->drawTable();
	}

}

class Columns {
	const PRODUCT_NAME = 42;
	const IMG = 21;
	const MODEL_NAME = 46;
}

class Item
{
	
	public $productName;
	public $img;
	public $modelName;

	public function __construct($item)
    {
    	$item = explode("\t", $item);

    	$this->productName = $item[Columns::PRODUCT_NAME];
    	$this->img = $item[Columns::IMG];
    	$this->modelName = $item[Columns::MODEL_NAME];
    }

    public function drawTitle()
    {
    	return '<tr>
		    <th>'.htmlspecialchars($this->productName).'</th>
			    <th>'.htmlspecialchars($this->img).'</th> 
			    <th>'.htmlspecialchars($this->modelName).'</th>
			</tr>';
	}

	public function drawRow()
    {
    	return '<tr>
		    <td>'.htmlspecialchars($this->productName).'</td>
			    <td>'.htmlspecialchars($this->img).'</td> 
			    <td>'.htmlspecialchars($this->modelName).'</td>
			</tr>';
	}
}

class Items 
{
	public $items = [];

	public function add(Item $item)
	{
		$this->items[] = $item;
	}

	public function drawTable()
	{
		echo '<table>';


		foreach ($this->items as $key => $item) {
			if ($key === 0) { //TODO
				echo $item->drawTitle();
				continue;
			}
			echo $item->drawRow();
		}
		echo '<table/>';
	}
}


$data = new CollectData(new CSV("http://www.printful.com/test.csv"));

$data->run();

$data->drawTable();